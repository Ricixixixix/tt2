<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ImageController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        return view('auth.image.index', compact('user'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function changeImage()
    {
        $user = Auth::user();
        return view('auth.image.uploadImage', compact('user'));
    }

    public function uploadImage(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', //validating uploaded image
        ]);
        $imageName = time().'.'.$request->image->extension(); //renaming to unique
        $request->image->move(public_path('images'), $imageName); //moving image with new name to public folder
        $user = Auth::user();
        $user->image = $imageName;
        $user->save(); //saving image name to user

        return back()
            ->with('success','Image for user was updated')
            ->with('image',$imageName);
    }

}
