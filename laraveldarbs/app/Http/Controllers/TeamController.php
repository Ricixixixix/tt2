<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Models\Tournament;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::all();
        return view('team.index', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::guest()) {
            if (Auth::user()->role == 'admin') {
                $allUsers = User::all();
                $users = $this->getTeamless($allUsers->all());

                return view('team.create', compact('users'));
            }
        }

        return redirect()->route('team.index')->with('error', 'Only admin can access create.');
    }

    public function getTeamless($users)
    {
        $teamless = [];
        foreach ($users as $user) {
            $count = DB::table('team_user')->get()->where('user_id', $user->id)->count();
            if ($count == 0) {
                array_push($teamless, $user);
            }
        }

        return $teamless;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if ($request->users == null || sizeof($request->users) != 6) {
            return redirect()->route('team.create')->with('error', 'Team must consist of six players!');
        }

        $teamName = [
            'name' => $request->name
        ];

        $team = Team::create($teamName);

        $users = $request->users;

        foreach ($users as $user) {
            $team->attachUser()->attach($user);//tiek saglabāts team user starptabulā
        }

        return redirect()->route('team.index')->with('success', 'Team was added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show(Team $team)//parāda komandu ar useriem
    {
        $playerIds = DB::table('team_user')->where('team_id', $team->id)->get('user_id');
        $players = [];

        foreach ($playerIds as $id) {
            array_push($players, User::find($id->user_id));
        }

        return view('team.show', compact('team', 'players'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(Team $team)//rediģēt komandu
    {
        $allUsers = User::all();
        $users = $this->getTeamless($allUsers->all());//nav komandā
        $teamPlayers = $this->getTeamMembers($team);//ir komandā

        foreach ($teamPlayers as $teamPlayer) {
            array_push($users, $teamPlayer);//apvieno vienā sarakstā
        }

        return view('team.edit', compact('users', 'teamPlayers', 'team'));
    }

    public function getTeamMembers($team)
    {
        $teamPlayers = [];

        $teamPlayerIds =  DB::table('team_user')->where('team_id', $team->id)->get('user_id');

        foreach ($teamPlayerIds as $teamPlayerId) {
            array_push($teamPlayers, User::find($teamPlayerId->user_id));
        }

        return $teamPlayers;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Team $team)
    {
        if ($request->users == null || sizeof($request->users) != 6) {
            return redirect()->route('team.edit', $team)->with('error', 'Team must consist of six players!');
        }

        $teamName = [
            'name' => $request->name
        ];

        $team->update($teamName);
        $this->removeDataBeforeUpdate($team);
        $users = $request->users;

        foreach ($users as $user) {
            $team->attachUser()->attach($user);
        }

        return redirect()->route('team.index')->with('success', 'Team was edited.');
    }

    public function removeDataBeforeUpdate($team)
    {
        DB::table('team_user')->where('team_id', $team->id)->delete();
    }

    public function deleteTournament($team) {
        $tournamentId = DB::table('team_tournament')->where('team_id', $team->id)->get('tournament_id')->first();
        app(TournamentController::class)->destroy(Tournament::find($tournamentId->tournament_id));
        DB::table('team_tournament')->where('tournament_id', $tournamentId->tournament_id)->delete();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Team $team)
    {
        $this->deleteTournament($team);
        $this->removeDataBeforeUpdate($team);
        $team->delete();

        return redirect()->route('team.index')->with('success', 'Team was successfully deleted.');
    }
}
