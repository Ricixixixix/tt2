<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Models\Tournament;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TournamentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()//parāda turnīru sarakstu
    {
        $tournaments = Tournament::all();
        $maps = DB::table('map')->get()->all();
        $teams = Team::all();
        $tournamentTeam = DB::table('team_tournament')->get()->all();

        return view('tournament.index', compact('tournaments', 'maps', 'teams', 'tournamentTeam'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()//atlasa komandas, karti
    {
        if (!Auth::guest()) {
            if (Auth::user()->role == 'admin') {
                $teams = Team::all();
                $maps = DB::table('map')->get()->all();
                return view('tournament.create', compact('teams', 'maps'));
            }
        }
        return redirect()->route('tournament.index')->with('error', 'Only admin can access create.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)//saglabā turnīru
    {
        $team1 = $request->team1;
        $team2 = $request->team2;

        if($team1 != $team2) {
            $tournamentData = [
                'name' => $request->name,
                'map_id' => $request->map_id
            ];

            $tournament = Tournament::create($tournamentData);

            $tournament->attachTeam()->attach($team1);
            $tournament->attachTeam()->attach($team2);

            return redirect()->route('tournament.index')->with('success', 'Tournament was added.');
        }

        return redirect()->route('tournament.create')->with('error', 'Same teams cant play with each other');
    }

    /**
     * @param Tournament $tournament
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getChangeStatsView(Tournament $tournament) {
        $players = User::all();
        $stats = DB::table('stats_user')->get()->all();
        $map = DB::table('map')->where('id', $tournament->map_id)->first();
        $teamIds = DB::table('team_tournament')->where('tournament_id', $tournament->id)->get('team_id')->all();
        $team1 = Team::find($teamIds[0]->team_id);
        $team2 = Team::find($teamIds[1]->team_id);
        $playersTeam1 = $this->getTeamsPlayers($team1, $players);
        $playersTeam2 = $this->getTeamsPlayers($team2, $players);

        return view('tournament.showPlayerStats', compact('tournament', 'playersTeam1', 'playersTeam2', 'stats', 'map', 'team1', 'team2'));
    }

    /**
     * @param Request $request
     * @param Tournament $tournament
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addStats(Request $request, Tournament $tournament): \Illuminate\Http\RedirectResponse
    {
        $teamIds = DB::table('team_tournament')->where('tournament_id', $tournament->id)->get('team_id')->all();
        $players = User::all();
        $team1 = Team::find($teamIds[0]->team_id);
        $team2 = Team::find($teamIds[1]->team_id);
        $playersTeam1 = $this->getTeamsPlayers($team1, $players);
        $playersTeam2 = $this->getTeamsPlayers($team2, $players);
        $this->addStatsToDB($playersTeam1, $request);
        $this->addStatsToDB($playersTeam2, $request);

        return redirect()->route('tournament.show', $tournament)->with('success', 'Tournament stats where refreshed.');

    }

    public function addStatsToDB($players, $request) {
        foreach ($players as $player) {
            $stats = [
                'user_id' => $player->id,
                'kills' => $request[sprintf('%skills', $player->id)],
                'assists' => $request[sprintf('%sassists', $player->id)],
                'deaths' => $request[sprintf('%sdeaths', $player->id)]
            ];

            DB::table('stats_user')->where('user_id', $player->id)->delete();
            DB::table('stats_user')->insert($stats);
        }
    }

    /**
     * @param Tournament $tournament
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function changeScore(Tournament $tournament) {
        return view('tournament.changeScore', compact('tournament'));
    }

    public function updateScore(Tournament $tournament, Request $request) {
        if (preg_match('/^[0-9]?[0-9][:][0-9]?[0-9]$/', $request->score)) {
            $tournament->update($request->all());

            return redirect()->route('tournament.show', $tournament)->with('success', 'Tournament score was refreshed.');
        }
        return redirect()->route('tournament.changeScore', $tournament)->with('error', 'Score is invalid.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tournament  $tournament
     * @return \Illuminate\Http\Response
     */
    public function show(Tournament $tournament)
    {
        $players = User::all();
        $stats = DB::table('stats_user')->get()->all();
        $map = DB::table('map')->where('id', $tournament->map_id)->first();
        $teamIds = DB::table('team_tournament')->where('tournament_id', $tournament->id)->get('team_id')->all();
        $team1 = Team::find($teamIds[0]->team_id);
        $team2 = Team::find($teamIds[1]->team_id);
        $playersTeam1 = $this->getTeamsPlayers($team1, $players);
        $playersTeam2 = $this->getTeamsPlayers($team2, $players);

        return view('tournament.show', compact('tournament', 'playersTeam1', 'playersTeam2', 'stats', 'map', 'team1', 'team2'));
    }

    /**
     * @param $team
     * @param $allPlayers
     * @return array
     */
    public function getTeamsPlayers($team, $allPlayers): array
    {
        $playerIds = DB::table('team_user')->where('team_id', $team->id)->get('user_id')->all();
        $players = [];
        foreach ($playerIds as $playerId) {
            foreach ($allPlayers as $player) {
                if($player->id == $playerId->user_id) {
                    array_push($players, $player);
                }
            }
        }

        return $players;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tournament  $tournament
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(Tournament $tournament)
    {
        $teams = Team::all();
        $maps = DB::table('map')->get()->all();
        $team1 = DB::table('team_tournament')->where('tournament_id', $tournament->id)->get()->first()->team_id;
        $team2 = DB::table('team_tournament')->where('tournament_id', $tournament->id)->get()->last()->team_id;

        return view('tournament.edit', compact('teams', 'maps', 'tournament', 'team1', 'team2'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tournament  $tournament
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Tournament $tournament)
    {
        $team1 = $request->team1;
        $team2 = $request->team2;

        if($team1 != $team2) {
            $tournamentData = [
                'name' => $request->name,
                'map_id' => $request->map_id
            ];

            $tournament->update($tournamentData);
            DB::table('team_tournament')->where('tournament_id', $tournament->id)->delete();
            $tournament->attachTeam()->attach($team1);
            $tournament->attachTeam()->attach($team2);

            return redirect()->route('tournament.index')->with('success', 'Tournament was updated.');
        }

        return redirect()->route('tournament.create')->with('error', 'Same teams cant play with each other');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tournament  $tournament
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Tournament $tournament)
    {
        $players = User::all();
        $tournamentTeamSql = DB::table('team_tournament')->where('tournament_id', $tournament->id);
        $tournamentTeamIds = $tournamentTeamSql->get('team_id')->all();
        $team1 = Team::find($tournamentTeamIds[0]->team_id);
        $team2 = Team::find($tournamentTeamIds[1]->team_id);
        $playersTeam1 = $this->getTeamsPlayers($team1, $players);
        $playersTeam2 = $this->getTeamsPlayers($team2, $players);
        $this->deletePlayerStats($playersTeam1);
        $this->deletePlayerStats($playersTeam2);
        $tournamentTeamSql->delete();
        $tournament->delete();

        return redirect()->route('tournament.index')->with('success', 'Tournament was deleted.');
    }

    /**
     * @param $players
     */
    public function deletePlayerStats($players) {
        foreach ($players as $player) {
            DB::table('stats_user')->where('user_id', $player->id)->delete();
        }
    }
}
