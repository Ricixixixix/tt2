<?php

use Illuminate\Support\Facades\DB;

function isInList($playerList, $playerId): bool
{
    foreach ($playerList as $player) {
        if($player->id == $playerId) {
            return true;
        }
    }
    return false;
}

function getMapName($maps, $teamId): string
{
    foreach ($maps as $map) {
        if($map->id == $teamId) {
            return $map->name;
        }
    }

    return 'Undefined';
}

function getTeamName($teams, $tournamentTeam, $tournamentId, $teamNum): string
{
    $twoTeams = [];

    foreach ($tournamentTeam as $tm) {
        if ($tm->tournament_id == $tournamentId) {
            array_push($twoTeams, $tm->team_id);
        }
    }

    if($teamNum == 1) {
        return getTeamNameById($teams, $twoTeams[0]);
    }

    return getTeamNameById($teams, $twoTeams[1]);
}

function getTeamNameById($teams, $teamId): string
{
    foreach ($teams as $team) {
        if ($team->id == $teamId) {
            return $team->name;
        }
    }

    return 'Undefined';
}

function getPlayerKills($player)
{
    $kills = DB::table('stats_user')->where('user_id', $player->id)->get('kills')->first();
    if (!empty($kills->kills)) {
        return (int)$kills->kills;
    }

    return 0;
}

function getPlayerAssists($player)
{
    $assists = DB::table('stats_user')->where('user_id', $player->id)->get('assists')->first();

    if (!empty($assists->assists)) {
        return (int)$assists->assists;
    }

    return 0;
}

function getPlayerDeaths($player)
{
    $deaths = DB::table('stats_user')->where('user_id', $player->id)->get('deaths')->first();

    if (!empty($deaths->deaths)) {
        return (int)$deaths->deaths;
    }

    return 0;
}
