<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'map_id',
        'score'
    ];

    protected $primaryKey = 'id';

    public function attachTeam() //function that adds users to model
    {
        return $this->belongsToMany(Team::class);
    }
}
