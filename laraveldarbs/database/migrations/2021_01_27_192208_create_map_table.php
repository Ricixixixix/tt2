<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });

       DB::table('map')->insert([
           ['name' => 'de_dust2'],
           ['name' => 'de_vertigo'],
           ['name' => 'de_nuke'],
           ['name' => 'de_inferno'],
           ['name' => 'de_train'],
           ['name' => 'de_overpass'],
           ['name' => 'de_mirage']
       ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map');
    }
}
