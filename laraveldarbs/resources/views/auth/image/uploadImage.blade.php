@extends('layouts.app')

@section('content')
    <div class="form-container">
        @csrf
        <h3>My profile</h3>
        <div class="form-group">
            <label>Name</label>
            <input class="form-control" type="text" id="name" name="name" required minlength="4" readonly value="{{ $user->name  }}">
        </div>
        <div class="form-group">
            <label>E-mail</label>
            <input class="form-control" type="email" id="email" name="email" required readonly value="{{ $user->email }}">
        </div>
        <a class="btn btn-info" href={{ route('image.changeImage')  }}>Change image</a>
            <form class="ImageUploader" action="{{ route('image.uploadImage')  }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <label>Image</label>
                        <input type="file" name="image" id="uploaded-image" class="form-control">
                    </div>
                    <div class="col-md-auto">
                        <button class="btn btn-outline-primary my-2 my-sm-0" type="submit" id="upload-profile">Add</button>
                    </div>
                </div>
            </form>
    </div>
@endsection
