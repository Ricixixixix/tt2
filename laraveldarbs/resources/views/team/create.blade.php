@extends('layouts.app')
@section('content')

<div class="form-container">
        <form class="new-challenge-form" method="post" enctype="multipart/form-data" action="{{ route('team.store') }}">
    @csrf
            <h3>New Team</h3>
            <div class="form-group">
                <label>Name</label>
                <input class="form-control" type="text" name="name" required minlength="4">
            </div>
            <div class="form-group">
                <label>Users</label>
                @if(json_encode($users) != '[]')
                <select id="user-select" name="users[]" multiple="multiple" class="form-control" >
                    @foreach($users as $user)
                        <option value="{{ $user->id }}">
                            {{ $user->name }}
                        </option>
                    @endforeach
                    @else
                        <div class="form-control">{{ __('There are no users') }}</div>
                @endif
                </select>
            </div>
            <button type="submit" class="btn btn-outline-primary my-2 my-sm-0">Submit</button>
        </form>
    </div>

@endsection
