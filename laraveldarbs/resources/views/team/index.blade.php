@extends('layouts.app')
@section('content')
    <a class="btn btn-success" href="{{ route('team.create')  }}">Create team</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Team</th>
            <th scope="col">Created at</th>
            <th scope="col">Updated at</th>
            <th scope="col">Options</th>
        </tr>
        </thead>
        <tbody>
        @foreach($teams as $team)
            <tr>
                <td>{{ $team->name }}</td>
                <td>{{ $team->created_at }}</td>
                <td>{{ $team->updated_at }}</td>
                <td>
                    <form action="{{ route('team.destroy',$team) }}" method="POST">
                        <a href="{{ route('team.show', $team ) }}" class="btn btn-success">Look</a>
                        @auth()
                            @if(Auth::user()->role == 'admin')
                                <a href="{{ route('team.edit', $team) }}" class="btn btn-info">Edit</a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            @endif
                        @endauth
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
