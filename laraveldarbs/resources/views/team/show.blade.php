@extends('layouts.app')
@section('content')

    <div class="form-container">
            <h1>{{ $team->name  }}</h1>
            <div class="form-group">
                <h3>Players</h3>
                @if(json_encode($players) != '[]')
                        @foreach($players as $player)
                            <div class="card" style="width: 18rem;">
                                <img class="card-img-top" src="{{ asset(sprintf('/images/%s', $player->image))  }}" alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text">{{ $player->name  }}.</p>
                                </div>
                            </div>
                        @endforeach
                   @else
                   <div class="form-control">{{ __('There are no players.') }}</div>
                @endif
            </div>
    </div>

@endsection
