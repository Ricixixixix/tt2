@extends('layouts.app')
@section('content')

    <div class="form-container">
        <form class="new-challenge-form" method="post" enctype="multipart/form-data" action="{{ route('tournament.updateScore', $tournament) }}">
            @csrf
            @method('PUT');
            <h3>Update score</h3>
            <div class="form-group">
                <label>Score</label>
                <input class="form-control" type="text" name="score" required minlength="3" value="{{ $tournament->score }}">
            </div>
            <button type="submit" class="btn btn-outline-primary my-2 my-sm-0">Submit</button>
        </form>
    </div>

@endsection
