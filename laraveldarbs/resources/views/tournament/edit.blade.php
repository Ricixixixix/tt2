@extends('layouts.app')
@section('content')

    <div class="form-container">
        <form class="new-challenge-form" method="post" enctype="multipart/form-data" action="{{ route('tournament.update', $tournament) }}">
            @csrf
            @method('PUT');
            <h3>Edit Tournament</h3>
            <div class="form-group">
                <label>Name</label>
                <input class="form-control" type="text" name="name" required minlength="4" value="{{ $tournament->name }}">
            </div>
            <div class="form-group">
                <label>Team 1</label>
                @if(json_encode($teams) != '[]')
                    <select class="form-select" name="team1" id="map-select">
                        @foreach($teams as $team)
                            <option value="{{ $team->id }}" {{ $team->id == $team1 ? 'selected' : ''   }}>
                                {{ $team->name }}
                            </option>
                        @endforeach
                        @else
                            <div class="form-control">{{ __('There are no teams') }}</div>
                        @endif
                    </select>
            </div>
            <div class="form-group">
                <label>Team 2</label>
                @if(json_encode($teams) != '[]')
                    <select class="form-select" name="team2" id="map-select">
                        @foreach($teams as $team)
                            <option value="{{ $team->id }}" {{ $team->id == $team2 ? 'selected' : ''   }}>
                                {{ $team->name }}
                            </option>
                        @endforeach
                        @else
                            <div class="form-control">{{ __('There are no teams') }}</div>
                        @endif
                    </select>
            </div>
            <div class="form-group">
                <label>Map</label>
                @if(json_encode($maps) != '[]')
                    <select class="form-select" name="map_id" id="map-select">
                        @foreach($maps as $map)
                            <option value="{{ $map->id }}" {{ $map->id == $tournament->map_id ? 'selected' : ''   }}>
                                {{ $map->name }}
                            </option>
                        @endforeach
                        @else
                            <div class="form-control">{{ __('There are no maps') }}</div>
                        @endif
                    </select>
            </div>
            <button type="submit" class="btn btn-outline-primary my-2 my-sm-0">Submit</button>
        </form>
    </div>

@endsection
