@extends('layouts.app')
@section('content')
    <a class="btn btn-success" href="{{ route('tournament.create')  }}">Create tournament</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Tournament</th>
            <th scope="col">Map</th>
            <th scope="col">Team 1</th>
            <th scope="col">Team 2</th>
            <th scope="col">Created at</th>
            <th scope="col">Updated at</th>
            <th scope="col">Options</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tournaments as $tournament)
            <tr>
                <td>{{ $tournament->name }}</td>
                <td>{{ getMapName($maps, $tournament->map_id) }}</td>
                <td>{{ getTeamName($teams, $tournamentTeam, $tournament->id, 1) }}</td>
                <td>{{ getTeamName($teams, $tournamentTeam, $tournament->id, 2) }}</td>
                <td>{{ $tournament->created_at }}</td>
                <td>{{ $tournament->updated_at }}</td>
                <td>
                    <form action="{{ route('tournament.destroy',$tournament) }}" method="POST">
                        <a href="{{ route('tournament.show', $tournament ) }}" class="btn btn-success">Look</a>
                        @auth()
                            {{--                            @if(Auth::user()->role == 'administrator')--}}
                            <a href="{{ route('tournament.edit', $tournament) }}" class="btn btn-info">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                            {{--                            @endif--}}
                        @endauth
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
