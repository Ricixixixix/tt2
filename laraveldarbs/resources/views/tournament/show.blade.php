@extends('layouts.app')
@section('content')
<div class="container">
    <a class="btn btn-primary" href="{{ route('tournament.getChangeStatsView', $tournament) }}">Change stats</a>
    <a class="btn btn-primary" href="{{ route('tournament.changeScore', $tournament) }}">Change score</a>
    <div class="row">
        <div class="col-sm">
            <h3>{{ $team1->name  }}</h3>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Player</th>
                    <th scope="col">Kills</th>
                    <th scope="col">Assists</th>
                    <th scope="col">Deaths</th>
                </tr>
                </thead>
                <tbody>
                @foreach($playersTeam1 as $player)
                    <tr>
                        <td>{{ $player->name }}</td>
                        <td>{{ getPlayerKills($player)  }}</td>
                        <td>{{ getPlayerAssists($player) }}</td>
                        <td>{{ getPlayerDeaths($player) }}</td>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-sm">
            <h3>{{ $team2->name }}</h3>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Player</th>
                    <th scope="col">Kills</th>
                    <th scope="col">Assists</th>
                    <th scope="col">Deaths</th>
                </tr>
                </thead>
                <tbody>
                @foreach($playersTeam2 as $player)
                    <tr>
                        <td>{{ $player->name }}</td>
                        <td>{{ getPlayerKills($player)  }}</td>
                        <td>{{ getPlayerAssists($player) }}</td>
                        <td>{{ getPlayerDeaths($player) }}</td>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <h1 class="score" style="width: 100%; text-align: center;">{{ $tournament->score }}</h1>
</div>
@endsection
