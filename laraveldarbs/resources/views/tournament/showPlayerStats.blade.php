@extends('layouts.app')
@section('content')
    <form class="new-challenge-form" method="post" enctype="multipart/form-data" action="{{ route('tournament.addStats', $tournament) }}">
        @csrf
        <button class="btn btn-success" type="submit">Save</button>
        <div class="row">
                <div class="col-sm">
                @foreach($playersTeam1 as $player)
                    <div class="form-group">
                        <label>{{ $player->name }}</label>
                        <br/>
                        <span>Kills</span>
                        <input class="form-control" type="number" name="{{ $player->id.'kills' }}" required min="0" value="{{ getPlayerKills($player) }}">
                        <span>Assists</span>
                        <input class="form-control" type="number" name="{{ $player->id.'assists' }}" required min="0" value="{{ getPlayerAssists($player) }}">
                        <span>Deaths</span>
                        <input class="form-control" type="number" name="{{ $player->id.'deaths' }}" required min="0" value="{{ getPlayerDeaths($player) }}">
                    </div>
                @endforeach
                </div>
                <div class="col-sm">
                    @foreach($playersTeam2 as $player)
                        <div class="form-group">
                            <label>{{ $player->name }}</label>
                            <br/>
                            <span>Kills</span>
                            <input class="form-control" type="number" name="{{ $player->id.'kills' }}" required min="0" value="{{ getPlayerKills($player) }}">
                            <span>Assists</span>
                            <input class="form-control" type="number" name="{{ $player->id.'assists' }}" required min="0" value="{{ getPlayerAssists($player) }}">
                            <span>Deaths</span>
                            <input class="form-control" type="number" name="{{ $player->id.'deaths' }}" required min="0" value="{{ getPlayerDeaths($player) }}">
                        </div>
                    @endforeach
                </div>
        </div>
    </form>
@endsection
