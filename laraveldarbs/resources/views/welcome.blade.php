@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>CS:GO tournaments</h1>
                In this laravel app you can create tournaments, teams, players, add stats to players and overall score!
            </div>
        </div>
    </div>
@endsection
