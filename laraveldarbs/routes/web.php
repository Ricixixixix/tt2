<?php

use App\Http\Controllers\Auth\ImageController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\TournamentController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::resource('/team', TeamController::class);
Route::resource('/tournament', TournamentController::class);
Route::post('/tournament/{tournament}/addStats', [TournamentController::class, 'addStats'])->name('tournament.addStats');
Route::get('/tournament/{tournament}/getChangeStatsView', [TournamentController::class, 'getChangeStatsView'])->name('tournament.getChangeStatsView');
Route::get('/tournament/{tournament}/changeScore', [TournamentController::class, 'changeScore'])->name('tournament.changeScore');
Route::put('/tournament/{tournament}/updateScore', [TournamentController::class, 'updateScore'])->name('tournament.updateScore');
Route::get('/image', [ImageController::class, 'index'])->name('image.index');
Route::get('/changeImage', [ImageController::class, 'changeImage'])->name('image.changeImage');
Route::post('/uploadImage', [ImageController::class, 'uploadImage'])->name('image.uploadImage');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
